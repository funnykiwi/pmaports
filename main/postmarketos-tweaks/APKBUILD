# Maintainer: Martijn Braam <martijn@brixit.nl>
pkgname=postmarketos-tweaks
pkgver=0.7.3
pkgrel=0
pkgdesc="Tweak tool for phone UIs"
url="https://gitlab.com/postmarketOS/postmarketos-tweaks"
arch="noarch"
license="GPL-3.0-or-later"
subpackages="$pkgname-phosh:phosh $pkgname-pinephone:pinephone"
depends="python3 py3-gobject3 py3-yaml gtk+3.0 libhandy1"
makedepends="py3-setuptools glib-dev libhandy1-dev meson"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://gitlab.com/postmarketOS/postmarketos-tweaks/-/archive/$pkgver/postmarketos-tweaks-$pkgver.tar.gz"
options="!check" # There's no testsuite

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"

	amove usr/share/postmarketos-tweaks/phosh.yml
}

pinephone() {
	install_if="$pkgname=$pkgver-r$pkgrel device-pine64-pinephone"

	amove usr/share/postmarketos-tweaks/pinephone.yml
}

sha512sums="
86ce03b4732a783609e99a439f801f571adb2b6041d5ffe38cde6084bad954d283dc37882d154bcd0397abfe360f957029eeb3493a17c285e2e71fe2b44eeeb2  postmarketos-tweaks-0.7.3.tar.gz
"
